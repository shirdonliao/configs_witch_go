//++++++++++++++++++++++++++++++++++++++++
//Fighting for great,share generate value!
//Build the best soft by golang,let's go!
//++++++++++++++++++++++++++++++++++++++++
//Author:ShirDon <http://www.shirdon.com>
//Email:hcbsts@163.com;  823923263@qq.com
//++++++++++++++++++++++++++++++++++++++++

package config

type SectionError string

func (e SectionError) Error() string {
	return "section not found: " + string(e)
}

type OptionError string

func (e OptionError) Error() string {
	return "option not found: " + string(e)
}
