configs_witch_go go语言配置包
======
想必gopher们都会抱怨，配置文件都读取每次都要自己封装，费时费力，有没有一个包来管理呢，下面这个包就免费分享给大家～
这个包实现了一种基本的配置文件解析器语言，提供类似于在Microsoft Windows ini上找到的结构文件夹。

例如:

	[My Section]
	foodir: %(dir)s/whatever
	dir=foo


## 安装方法

	go get gitee.com/shirdonl/configs_with_go

## 操作说明

新建一个简单都配置文件:

	[DEFAULT]
	host: www.example.com
	protocol: http://
	base-url: %(protocol)s%(host)s

	[service-1]
	url: %(base-url)s/some/path
	delegation: on
	maxclients: 200 # do not set this higher
	comments: This is a multi-line
		entry	# And this is a comment

如果要执行配置文件，放入如下:

	c, _ := config.ReadDefault("config.cfg")

	c.String("service-1", "url")
	// result is string "http://www.example.com/some/path"

	c.Int("service-1", "maxclients")
	// result is int 200

	c.Bool("service-1", "delegation")
	// result is bool true

	c.String("service-1", "comments")
	// result is string "This is a multi-line\nentry"

请注意对展开变量（如*%（基URL）s*）的支持，这些变量是读取的从特殊（保留）节名称*[默认]*。



还可以使用以下命令创建新的配置文件：

	c := config.NewDefault()
	c.AddSection("Section")
	c.AddOption("Section", "option", "value")
	c.WriteFile("config.cfg", 0644, "A header for this file")

结果将在如下文件:

	# A header for this file

	[Section]
	option: value

这样大家就可以很快都全局调用配置信息了～
注意，值都区分大小写。

## 开源协议

[Mozilla Public License, version 2.0](http://mozilla.org/MPL/2.0/),


